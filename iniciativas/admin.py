from django.contrib import admin
from .models import Etiqueta, Iniciativa

class IniciativaAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Historial',        {'fields': ['fecha_pre','fecha_apr','solicitante']}),
        ('Descripción',      {'fields': ['nombre','descripcion','objetivo','beneficio','impacto_no']}),
        ('Etiquetas',        {'fields': ['etiquetas']}),
    ]

admin.site.register(Etiqueta)
admin.site.register(Iniciativa, IniciativaAdmin)