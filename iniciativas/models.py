from django.db import models
from django.utils import timezone

class Etiqueta(models.Model):
    nombre = models.CharField(max_length=32)
    descripcion = models.CharField('descripción', max_length=300, blank=True)
    def __str__(self):
        return self.nombre

class Iniciativa(models.Model):
    nombre = models.CharField(max_length=75)
    descripcion = models.CharField('descripción', max_length=150)
    solicitante = models.CharField(max_length=100)
    objetivo = models.CharField(max_length=300)
    beneficio = models.CharField(max_length=300)
    impacto_no = models.CharField('impacto de no ejecutar', max_length=300)
    fecha_pre = models.DateTimeField('fecha de presentación')
    fecha_apr = models.DateTimeField('fecha de aprobación', blank=True)
    etiquetas = models.ManyToManyField(Etiqueta)
    def __str__(self):
        return self.nombre
### solicitante debe ser una referencia a una lista de gerencias, esa app
### sería externa así que de momento es solo una cadena de texto.